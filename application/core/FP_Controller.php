<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class FP_Controller extends MX_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'config/m_config',
    ));
  }

  function render($content, $data = NULL)
  {
    // Templating
    $data['header'] = $this->load->view('template/public/header', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);
    $data['footer'] = $this->load->view('template/public/footer', $data, TRUE);

    $this->load->view('template/public/index', $data);
  }
}
