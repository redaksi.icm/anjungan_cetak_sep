<script type="text/javascript">
  $(document).ready(function() {
    Informasi('<?= @$no_rujukan ?>');
  });

  function Informasi(no_rujukan) {
    $('#info-peserta').html('<tr><td colspan="3" class="text-center"><div class="mt-2"><i class="fas fa-spinner fa-spin fa-2x"></i><br>Memuat...</div></td></td>');
    $('#info-rujukan').html('<tr><td colspan="3" class="text-center"><div class="mt-2"><i class="fas fa-spinner fa-spin fa-2x"></i><br>Memuat...</div></td></td>');
    $.ajax({
      url: '<?= site_url('detail_rujukan/detail_rujukan/informasi') ?>',
      type: 'POST',
      dataType: 'json',
      data: {
        'no_rujukan': no_rujukan
      },
      error: function() {
        $('#info-peserta').html('<tr><td colspan="3" class="text-center"><i class="fas fa-exclamation-circle"></i> Gagal memuat data, silahkan klik tombol <b>MUAT ULANG</b> untuk memuat ulang data</td></td>');
        $('#info-rujukan').html('<tr><td colspan="3" class="text-center"><i class="fas fa-exclamation-circle"></i> Gagal memuat data, silahkan klik tombol <b>MUAT ULANG</b> untuk memuat ulang data</td></td>');
      },
      success: function(data) {
        $('#info-peserta').html(data.info_peserta);
        $('#info-rujukan').html(data.info_rujukan);
      },
      timeout: 180000 // 3 menit
    });
  }
</script>