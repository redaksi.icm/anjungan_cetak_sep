<?php if (@$code == 200) : ?>
  <tr>
    <th class="text-left" width="150">No. Rujukan</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['noKunjungan'] ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Tgl. Rujukan</th>
    <th class="text-center" width="3">:</th>
    <td><?= to_date(@$main['tglKunjungan']) ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Diagnosa Awal</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['diagnosa']['kode'] ?> - <?= @$main['diagnosa']['nama'] ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Keluhan</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['keluhan'] ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Poli Tujuan</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['poliRujukan']['nama'] ?></td>
  </tr>
<?php else : ?>
  <tr>
    <td colspan="3" class="text-center"><?= @$message ?></td>
  </tr>
<?php endif; ?>