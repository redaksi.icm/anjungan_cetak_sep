<?php if (@$code == 200) : ?>
  <tr>
    <th class="text-left" width="150">No.Kartu BPJS</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['peserta']['noKartu'] ?> ( MR. <?= @$main['peserta']['mr']['noMR'] ?> )</td>
  </tr>
  <tr>
    <th class="text-left" width="150">NIK</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['peserta']['nik'] ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Nama Peserta</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['peserta']['nama'] ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Jenis Kelamin</th>
    <th class="text-center" width="3">:</th>
    <td>
      <?php if (@$main['peserta']['sex'] == 'L') : ?>
        Laki-Laki
      <?php elseif (@$main['peserta']['sex'] == 'P') : ?>
        Perempuan
      <?php else : ?>
        -
      <?php endif; ?>
    </td>
  </tr>
  <tr>
    <th class="text-left" width="150">Tgl. Lahir</th>
    <th class="text-center" width="3">:</th>
    <td><?= to_date(@$main['peserta']['tglLahir']) ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">No. Telepon</th>
    <th class="text-center" width="3">:</th>
    <td><?= @$main['peserta']['mr']['noTelepon'] ?></td>
  </tr>
  <tr>
    <th class="text-left" width="150">Peserta</th>
    <th class="text-center" width="3">:</th>
    <td>-</td>
  </tr>
<?php else : ?>
  <tr>
    <td colspan="3" class="text-center"><?= @$message ?></td>
  </tr>
<?php endif; ?>