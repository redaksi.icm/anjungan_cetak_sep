<style>
  #frame_pdf {
    width: 100%;
    min-height: 68vh;
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
    GetRujukan('<?= @$no_rujukan ?>');
  });

  function GetRujukan(no_rujukan) {
    $('#box-data').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Memuat...</div>');
    $.ajax({
      url: '<?= site_url('detail_rujukan/detail_rujukan/get_rujukan') ?>',
      type: 'POST',
      dataType: 'json',
      data: {
        'no_rujukan': no_rujukan
      },
      error: function() {
        $('#box-data').html('<div class="text-center mb-4"><i class="fas fa-exclamation-circle"></i> Gagal memuat data, silahkan klik tombol <b>MUAT ULANG</b> untuk memuat ulang data<div>');
      },
      success: function(data) {
        if (data.code == 200) {
          $('#box-data').html('<iframe id="frame_pdf" src="<?= $url ?>" frameborder="0"></iframe><a href="<?= site_url('detail_rujukan/detail_rujukan/index/' . @$no_kartu . '/' . @$no_rujukan) ?>" class="float-right btn btn-primary mt-1"><i class="fas fa-print"></i> Cetak SEP</a>');
        } else {
          $('#box-data').html('<div class="text-center mb-3"><h5>' + data.message + '</h5></div>');
        }
      },
      timeout: 180000 // 3 menit
    });
  }
</script>
<div class="content-body">
  <div class="row">
    <div class="col-md-12">
      <div id="box-data"></div>
      <button type="button" class="float-right btn btn-secondary btn-cancel mt-1 mr-2" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>