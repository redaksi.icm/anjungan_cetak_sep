<?php $this->load->view('_js') ?>
<div class="container pt-4">
  <div class="row justify-content-center text-center">
    <div class="col-12 col-md-12 col-lg-8 mb-8 mb-lg-0">
      <div class="mb-n1">
        <h2 class="mt-4 font-weight-bold text-white">LOREM IPSUM DOLOR SIT AMET</h2>
        <p class="lead mb-0 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  </div>
  <!-- / .row -->
</div>
<div class="container">
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <div class="d-flex justify-content-center mb-3">
          <a href="<?= site_url('rujukan/list_rujukan/index/' . @$no_kartu) ?>" class="btn btn-warning text-white shadow mr-1 my-2 my-lg-0 my-md-0">
            <i class="fas fa-chevron-circle-left"></i> KEMBALI
          </a>
          <a href="<?= site_url('detail_rujukan/detail_rujukan/index/' . @$no_kartu . '/' . @$no_rujukan) ?>" class="btn btn-warning text-white shadow mr-1 my-2 my-lg-0 my-md-0">
            <i class="fas fa-sync-alt"></i> MUAT ULANG
          </a>
          <a href="#" data-href="<?= site_url('detail_rujukan/detail_rujukan/cetak_modal/cetak_pdf/' . @$no_kartu . '/' . @$no_rujukan) ?>" modal-title="Cetak SEP" modal-size="lg" class="btn btn-warning text-white shadow mr-3 my-2 my-lg-0 my-md-0 modal-href">
            <i class="fas fa-print"></i> CETAK SEP
          </a>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="table-responsive">
          <table class="table table-striped table-light table-hover shadow">
            <tbody id="info-peserta"></tbody>
          </table>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="table-responsive">
          <table class="table table-striped table-light table-hover shadow">
            <tbody id="info-rujukan">
            </tbody>
          </table>
        </div>
        <div class="card p-3 shadow border-0 flex-sm-row mt-2">
          <div class="card-body p-0 mb-n4">
            <form>
              <div class="form-group row">
                <label class="col-sm-4 col-form-label mt-1 font-weight-bold">Nomor SKDP</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control">
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div style="position: fixed; bottom: 0; width: 100%; z-index: -10">
  <img src="<?= base_url() ?>images/icon/footer-img.png" style="width: 100%; margin-top: -10em;">
</div>