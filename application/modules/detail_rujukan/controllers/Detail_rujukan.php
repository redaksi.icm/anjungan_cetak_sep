<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detail_rujukan extends FP_Controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'm_detail_rujukan'
    ));
  }

  public function index($no_kartu = null, $no_rujukan = null)
  {
    $data['no_kartu'] = @$no_kartu;
    $data['no_rujukan'] = @$no_rujukan;
    $this->render('index', $data);
  }

  public function informasi()
  {
    $no_rujukan = $this->input->post('no_rujukan');
    //
    $vclaim_conf = [
      'cons_id' => '24940',
      'secret_key' => '0qN6CB85B0',
      'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
      'service_name' => 'new-vclaim-rest'
      // 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
      // 'service_name' => 'vclaim-rest'
    ];

    $peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
    $result = $peserta->cariByNoRujukan('', $no_rujukan);

    $data['no_rujukan'] = @$no_rujukan;
    $data['code'] = @$result['metaData']['code'];
    $data['message'] = @$result['metaData']['message'];
    if (@$result['metaData']['code'] == 200) {
      $data['main'] = $result['response']['rujukan'];
    }

    echo json_encode(array(
      'info_peserta' => $this->load->view('info_peserta', $data, true),
      'info_rujukan' => $this->load->view('info_rujukan', $data, true)
    ));
  }

  public function get_rujukan()
  {
    $no_rujukan = $this->input->post('no_rujukan');
    //
    $vclaim_conf = [
      'cons_id' => '24940',
      'secret_key' => '0qN6CB85B0',
      'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
      'service_name' => 'new-vclaim-rest'
      // 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
      // 'service_name' => 'vclaim-rest'
    ];

    $peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
    $result = $peserta->cariByNoRujukan('', $no_rujukan);

    $data['no_rujukan'] = @$no_rujukan;
    $data['code'] = @$result['metaData']['code'];
    $data['message'] = @$result['metaData']['message'];
    if (@$result['metaData']['code'] == 200) {
      $data['main'] = $result['response']['rujukan'];
    }

    echo json_encode($data);
  }

  public function cetak_modal($type = null, $no_kartu = null, $no_rujukan = null)
  {
    $data['no_kartu'] = @$no_kartu;
    $data['no_rujukan'] = @$no_rujukan;
    $data['url'] = site_url() . '/detail_rujukan/detail_rujukan/' . $type . '/' . @$no_kartu . '/' . @$no_rujukan;

    echo json_encode(array(
      'html' => $this->load->view('detail_rujukan/cetak_modal', $data, true)
    ));
  }

  public function cetak_pdf($no_kartu = null, $no_rujukan = null)
  {
    $vclaim_conf = [
      'cons_id' => '24940',
      'secret_key' => '0qN6CB85B0',
      'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
      'service_name' => 'new-vclaim-rest'
      // 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
      // 'service_name' => 'vclaim-rest'
    ];

    $peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
    $result = $peserta->cariByNoRujukan('', $no_rujukan);

    if (@$result['metaData']['code'] == 200) {
      $main = $result['response']['rujukan'];
    }

    if (@$main['peserta']['sex'] == 'L') {
      $jns_kelamin = 'Laki-Laki';
    } elseif (@$main['peserta']['sex'] == 'P') {
      $jns_kelamin = 'Perempuan';
    } else {
      $jns_kelamin = '';
    }

    if (@$main['peserta']['cob']['noAsuransi'] != '') {
      $no_asuransi = @$main['peserta']['cob']['noAsuransi'];
    } else {
      $no_asuransi = '-';
    }

    //generate pdf
    $this->load->library('pdf');
    $pdf = new Pdf('p', 'mm', array(210, 297));
    $pdf->AliasNbPages();
    $pdf->SetTitle('Cetak SEP ');
    $pdf->SetMargins(2, 2, 2);
    $pdf->AddPage();

    $pdf->Image(FCPATH . 'images/icon/bpjs.png', 3, 3, 50, 8);
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(60, 2, '', 0, 1, 'L');
    $pdf->Cell(60, 6, '', 0, 0, 'L');
    $pdf->Cell(0, 3, 'SURAT ELEGIBILITAS PESERTA', 0, 1, 'L');
    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(60, 6, '', 0, 0, 'L');
    $pdf->Cell(0, 5, 'RSUD DR. SOEDIRMAN KEBUMEN', 0, 1, 'L');
    $pdf->SetFont('Arial', '', 12);
    $pdf->Cell(0, 5, '', 0, 1, 'C');

    $pdf->SetFont('Arial', '', 10);
    $pdf->Cell(30, 5, 'No.SEP', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['noKunjungan'], 0, 1, 'L');
    $pdf->Cell(30, 5, 'Tgl.SEP', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['tglKunjungan'], 0, 0, 'L');
    $pdf->Cell(30, 5, 'Peserta', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(73, 5, '-', 0, 1, 'L');
    $pdf->Cell(30, 5, 'No.Kartu', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['peserta']['noKartu'] . ' ( MR. ' . @$main['peserta']['mr']['noMR'] . ' )', 0, 0, 'L');
    $pdf->Cell(30, 5, 'COB', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(73, 5, @$no_asuransi, 0, 1, 'L');
    $pdf->Cell(30, 5, 'Nama Peserta', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['peserta']['nama'], 0, 0, 'L');
    $pdf->Cell(30, 5, 'Jns.Rawat', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(73, 5, 'R.Inap', 0, 1, 'L');
    $pdf->Cell(30, 5, 'Tgl.Lahir', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['peserta']['tglLahir'] . ' Kelamin : ' . @$jns_kelamin, 0, 0, 'L');
    $pdf->Cell(30, 5, 'Kls.Rawat', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(73, 5, @$main['peserta']['hakKelas']['keterangan'], 0, 1, 'L');
    $pdf->Cell(30, 5, 'No.Telepon', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['peserta']['mr']['noTelepon'], 0, 0, 'L');
    $pdf->Cell(30, 5, 'Penjamin', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, '', 0, 1, 'L');
    $pdf->Cell(30, 5, 'Sub/Spesialis', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, '-', 0, 1, 'L');
    $pdf->Cell(30, 5, 'Faskes Perujuk', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['provPerujuk']['nama'], 0, 1, 'L');
    $pdf->Cell(30, 5, 'Diagnosa Awal', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 5, @$main['diagnosa']['kode'] . ' - ' . @$main['diagnosa']['nama'], 0, 1, 'L');
    $pdf->Cell(30, 5, 'Catatan', 0, 0, 'L');
    $pdf->Cell(2, 5, ':', 0, 0, 'C');
    $pdf->Cell(90, 7, '', 0, 1, 'L');

    $pdf->SetFont('Arial', '', 8);
    $pdf->Cell(150, 3.5, '*Saya menyetujui BPJS Kesehatan menggunakan informasi medis pasien jika diperlukan.', 0, 0, 'L');
    $pdf->Cell(60, 3.5, 'Pasien/Keluarga Pasien', 0, 1, 'C');
    $pdf->Cell(150, 3.5, '*SEP Bukan sebagai bukti penjaminan peserta', 0, 1, 'L');

    $pdf->SetFont('Arial', '', 8);
    $pdf->Cell(90, 5, '', 0, 1, 'L');
    $pdf->Cell(150, 3.5, 'Cetakan ke 1 ' . to_date(date('Y-m-d H:i:s'), '', 'full_date') . ' WIB', 0, 1, 'L');
    $pdf->Cell(90, 5, '', 0, 1, 'L');

    $pdf->Output('I', 'SEP_' . '_' . date('Ymdhis') . '.pdf');
  }
}
