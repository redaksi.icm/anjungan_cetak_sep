<script type="text/javascript">
  $(document).ready(function() {
    $("#search").click(function() {
      if ($("#no_kartu").val() == '') {
        swal({
          title: "Nomor Kartu BPJS masih kosong !",
          text: "Silahkan isi Nomor Kartu BPJS Anda",
          icon: "warning",
          button: "OK",
        });
        return false;
      } else {
        $("#search").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#search").attr("disabled", "disabled");
        $("#form").submit();
      }
    });
  });
</script>