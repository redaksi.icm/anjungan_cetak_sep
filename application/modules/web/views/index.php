<?php $this->load->view('_js') ?>
<!--hero section start-->
<div class="bg-primary position-relative d-flex align-items-center" style="height: 100vh;">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-12 col-lg-5 col-xl-6 order-lg-2 mb-8 mb-lg-0">
        <!-- Image -->
        <img src="<?= base_url() ?>public/images/hero/02.png" class="img-fluid" alt="...">
      </div>
      <div class="col-12 col-lg-7 col-xl-6 order-lg-1">
        <!-- Heading -->
        <h1 class="display-4 text-white mt-3 font-weight-semibold">
          CETAK SEP MANDIRI
        </h1>
        <!-- Text -->
        <p class="lead text-light">Masukkan Nomor Kartu BPJS Anda</p>
        <div class="subscribe-form border rounded p-3">
          <form class="group" id="form" action="<?= site_url() . '/web/index/search' ?>" method="post" autocomplete="off">
            <div class="d-sm-flex align-items-center form-group mb-0">
              <input type="number" name="no_kartu" id="no_kartu" class="form-control" placeholder="Contoh : 0123456789123" style="height: 4rem; font-size: 1.375rem;">
              <button class="btn btn-info ml-sm-2 mt-2 mt-sm-0" id="search" type="submit" style="padding: 19px 20px; width:22vh;"><i class="fas fa-search"></i> Cari</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- / .row -->
  </div>
  <!-- / .container -->
</div>

<!--hero section end-->