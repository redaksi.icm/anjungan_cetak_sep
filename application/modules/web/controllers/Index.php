<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends FP_Controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'm_index'
    ));
  }

  public function index()
  {
    $data['main'] = array();
    $this->render('index', $data);
  }

  public function search()
  {
    $no_kartu = $this->input->post('no_kartu');

    redirect(site_url() . '/rujukan/list_rujukan/index/' . $no_kartu);
  }

  function testing($no_kartu = null)
  {
    $vclaim_conf = [
      'cons_id' => '24940',
      'secret_key' => '0qN6CB85B0',
      'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
      'service_name' => 'new-vclaim-rest'
      // 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
      // 'service_name' => 'vclaim-rest'
    ];

    $result = array();
    $peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
    $data = $peserta->cariByNoKartu('', $no_kartu, true);

    echo json_encode($data);
  }

  function rujukan_by_no_rujukan($no_rujukan = null)
  {
    $vclaim_conf = [
      'cons_id' => '24940',
      'secret_key' => '0qN6CB85B0',
      'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
      'service_name' => 'new-vclaim-rest'
      // 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
      // 'service_name' => 'vclaim-rest'
    ];

    $result = array();
    $peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
    $data = $peserta->cariByNoRujukan('', $no_rujukan);

    echo json_encode($data);
  }
}
