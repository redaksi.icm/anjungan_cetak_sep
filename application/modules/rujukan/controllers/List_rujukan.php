<?php
defined('BASEPATH') or exit('No direct script access allowed');

class List_rujukan extends FP_Controller
{

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'm_list_rujukan'
    ));
  }

  public function index($no_kartu = null)
  {
    $data['no_kartu'] = $no_kartu;
    $this->render('index', $data);
  }

  public function list_rujukan()
  {
    $no_kartu = $this->input->post('no_kartu');
    //
    $vclaim_conf = [
      'cons_id' => '24940',
      'secret_key' => '0qN6CB85B0',
      'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
      'service_name' => 'new-vclaim-rest'
      // 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
      // 'service_name' => 'vclaim-rest'
    ];

    $peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
    $result = $peserta->cariByNoKartu('', $no_kartu, true);

    $data['no_kartu'] = @$no_kartu;
    $data['code'] = @$result['metaData']['code'];
    $data['message'] = @$result['metaData']['message'];
    if (@$result['metaData']['code'] == 200) {
      $data['main'] = $result['response']['rujukan'];
    }

    echo json_encode(array(
      'html' => $this->load->view('list_rujukan', $data, true)
    ));
  }
}
