<script type="text/javascript">
  $(document).ready(function() {
    ListRujukan('<?= @$no_kartu ?>');
  });

  function ListRujukan(no_kartu) {
    $('#list-rujukan').html('<tr><td colspan="99" class="text-center"><div class="mt-2"><i class="fas fa-spinner fa-spin fa-2x"></i><br>Memuat...</div></td></td>');
    $.ajax({
      url: '<?= site_url('rujukan/list_rujukan/list_rujukan') ?>',
      type: 'POST',
      dataType: 'json',
      data: {
        'no_kartu': no_kartu
      },
      error: function() {
        $('#list-rujukan').html('<tr><td colspan="99" class="text-center"><i class="fas fa-exclamation-circle"></i> Gagal memuat data, silahkan klik tombol <b>MUAT ULANG</b> untuk memuat ulang data</td></td>');
      },
      success: function(data) {
        $('#list-rujukan').html(data.html);
      },
      timeout: 180000 // 3 menit
    });
  }
</script>