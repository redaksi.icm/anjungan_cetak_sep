<?php if (@$code == 200) : ?>
  <?php $no = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-left"><?= $no++ ?></td>
      <td class="text-center"><?= $row['noKunjungan'] ?></td>
      <td><?= $row['peserta']['nama'] ?></td>
      <td><?= $row['poliRujukan']['nama'] ?></td>
      <td class="text-center">
        <a href="<?= site_url('detail_rujukan/detail_rujukan/index/' . @$no_kartu . '/' . @$row['noKunjungan']) ?>" class="btn btn-primary btn-sm"><i class="fas fa-print"></i> Cetak</a>
      </td>
    </tr>
  <?php endforeach; ?>
<?php else : ?>
  <tr>
    <td colspan="99" class="text-center"><?= @$message ?></td>
  </tr>
<?php endif; ?>