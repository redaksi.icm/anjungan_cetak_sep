<?php $this->load->view('_js') ?>
<div class="container pt-4">
  <div class="row justify-content-center text-center">
    <div class="col-12 col-md-12 col-lg-8 mb-8 mb-lg-0">
      <div class="mb-n1">
        <h2 class="mt-4 font-weight-bold text-white">LOREM IPSUM DOLOR SIT AMET</h2>
        <p class="lead mb-0 text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
      </div>
    </div>
  </div>
  <!-- / .row -->
</div>
<div class="container">
  <div class="card-body">
    <div class="d-flex justify-content-center mb-3">
      <a href="<?= site_url() ?>" class="btn btn-warning text-white shadow mr-1 my-2 my-lg-0 my-md-0">
        <i class="fas fa-chevron-circle-left"></i> KEMBALI
      </a>
      <a href="<?= site_url('rujukan/list_rujukan/index/' . @$no_kartu) ?>" class="btn btn-warning text-white shadow mr-1 my-2 my-lg-0 my-md-0">
        <i class="fas fa-sync-alt"></i> MUAT ULANG
      </a>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-light table-hover">
        <thead>
          <tr>
            <th class="text-left" width="30">NO</th>
            <th class="text-center" width="200">NO. RUJUKAN</th>
            <th class="text-left">NAMA</th>
            <th class="text-left" width="300">POLI TUJUAN</th>
            <th class="text-center" width="100">AKSI</th>
          </tr>
        </thead>
        <tbody id="list-rujukan"></tbody>
      </table>
    </div>
  </div>
</div>

<div style="position: fixed; bottom: 0; width: 100%; z-index: -10">
  <img src="<?= base_url() ?>images/icon/footer-img.png" style="width: 100%; margin-top: -10em;">
</div>