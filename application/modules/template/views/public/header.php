<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="keywords" content="bootstrap 4, premium, multipurpose, sass, scss, saas" />
  <meta name="description" content="Bootstrap 4 Landing Page Template" />
  <meta name="author" content="www.themeht.com" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Anjungan Cetak SEP - RSUD Dr. Soedirman Kebumen</title>
  <link rel="shortcut icon" href="<?= base_url() ?>images/icon/favicon.ico" />
  <link href="<?= base_url() ?>public/css/fonts.googleapis.css" rel="stylesheet">
  <link href="http://localhost/jts/smartnaker_klaten/assets/public/css/fontawesome.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>public/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?= base_url() ?>public/css/magnific-popup.css" rel="stylesheet" />
  <link href="<?= base_url() ?>public/css/jquery-ui.css" rel="stylesheet" />
  <link href="<?= base_url() ?>public/css/spacing.css" rel="stylesheet" />
  <link href="<?= base_url() ?>public/css/theme.min.css" rel="stylesheet" />

  <script src="<?= base_url() ?>public/js/jquery.min.js"></script>
  <script src="<?= base_url() ?>public/js/bootstrap.min.js"></script>
  <script src="<?= base_url() ?>public/js/theme-script.js"></script>
  <script src="<?= base_url() ?>public/js/sweetalert.min.js"></script>
  <script src="<?= base_url() ?>public/js/custom.js"></script>
</head>

<body class="bg-primary">
  <!-- page wrapper start -->
  <div class="page-wrapper">