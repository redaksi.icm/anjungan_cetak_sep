</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog" aria-hidden="true">
  <div id="modal-size" class="modal-dialog" role="document">
    <div class="modal-content modal-content-top">
      <div class="modal-header" id="modal-header">
        <div class="modal-title" id="modal-title"></div>
        <button type="button" class="close btn-no-border" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->
</body>

</html>