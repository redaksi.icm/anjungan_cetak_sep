/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : anjungan_cetak_sep

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 01/10/2020 09:36:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for access
-- ----------------------------
DROP TABLE IF EXISTS `access`;
CREATE TABLE `access`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `access` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'System',
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of access
-- ----------------------------
INSERT INTO `access` VALUES (1, 'read', 'Mengakses menu.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (2, 'create', 'Menambah data.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (3, 'update', 'Mengubah data.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (4, 'delete', 'Menghapus data.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (5, 'report', 'Membuat laporan. ', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (6, 'login', 'Masuk ke aplikasi.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (7, 'loginfailed', 'Gagal masuk ke aplikasi.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `access` VALUES (8, 'logout', 'Keluar dari aplikasi.', NULL, 'System', NULL, NULL, NULL, NULL, 0, 1);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `menu_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `parent_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 1,
  `menu_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `controller` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `icon` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `url` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `is_create` tinyint(1) NOT NULL DEFAULT 0,
  `is_update` tinyint(1) NOT NULL DEFAULT 0,
  `is_delete` tinyint(1) NOT NULL DEFAULT 0,
  `is_report` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'System',
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('01', '', 3, 'Dashboard', 'dashboard', 'fas fa-tachometer-alt', 'index', 1, 0, 0, 0, 0, '2019-07-04 11:45:32', 'System', '2020-09-24 11:27:44', NULL, NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('99', '', 2, 'Pengaturan', '#', 'fas fa-list', '#', 1, 0, 0, 0, 0, '2019-07-05 05:35:21', 'System', '2020-01-10 08:23:36', 'Super Administrator', NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('99.01', '99', 3, 'Menu', 'menus', 'fas fa-bars', 'index', 1, 1, 1, 1, 0, '2019-07-05 05:38:26', 'System', '2020-01-10 08:22:30', 'Super Administrator', NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('99.02', '99', 3, 'Role', 'roles', 'fas fa-users', 'index', 1, 1, 1, 1, 0, '2019-07-16 07:15:08', 'Super Administrator', '2020-01-10 07:35:14', 'Super Administrator', NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('99.03', '99', 3, 'Pengguna', 'users', 'fas fa-user', 'index', 1, 1, 1, 1, 0, '2019-07-17 04:12:26', 'Super Administrator', '2020-09-24 15:02:24', NULL, NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('99.04', '99', 3, 'Profil', 'profile', 'fas fa-address-card', 'form', 1, 0, 1, 0, 0, '2019-07-21 14:11:08', 'Super Administrator', '2020-01-10 13:00:40', 'Super Administrator', NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('91.01', '91', 3, 'Parameter', '_parameter', 'fas fa-list', 'index', 1, 1, 1, 1, 0, '2020-01-10 12:01:11', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `menus` VALUES ('99.05', '99', 3, 'Tema', 'theme', 'fas fa-image', 'form', 1, 0, 1, 0, 0, '2020-01-10 13:00:26', 'Super Administrator', '2020-09-27 19:25:54', 'Super Administrator', NULL, NULL, 0, 1);

-- ----------------------------
-- Table structure for profile
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile`  (
  `app_id` varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `app_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `short_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `company_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fax` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `logo` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `version` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `start_year` int NOT NULL DEFAULT 0,
  `navbar_variant` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `brand_logo_variant` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `sidebar_variant` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `sidebar_accent` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `navbar_border` tinyint(1) NOT NULL DEFAULT 0,
  `body_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `navbar_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `sidebarnav_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `footer_small_text` tinyint(1) NOT NULL DEFAULT 0,
  `flat_sidebar` tinyint(1) NOT NULL DEFAULT 0,
  `legacy_sidebar` tinyint(1) NOT NULL DEFAULT 0,
  `compact_sidebar` tinyint(1) NOT NULL DEFAULT 0,
  `nav_child_indent` tinyint(1) NOT NULL DEFAULT 0,
  `layout_fixed` tinyint(1) NOT NULL,
  `layout_navbar_fixed` tinyint(1) NOT NULL,
  `layout_footer_fixed` tinyint(1) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'System',
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`app_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of profile
-- ----------------------------
INSERT INTO `profile` VALUES ('0bb9067a-17f2-43d0-b97b-41549905b28a', 'Ignite Framework', 'IGNITE FW', 'Farras Soft', 'Condrowangsan RT 10, Potorono, Banguntapan', '085326694224', '-', 'farras.soft@gmail.com', 'Lorem ipsum dolor sit amet', 'logo.png', '0.1.0', 2020, 'navbar-dark navbar-primary', 'navbar-primary', 'sidebar-dark-', 'primary', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '2019-07-02 07:41:39', 'System', '2020-09-27 19:38:14', 'Super Administrator', NULL, NULL, 0, 0);

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu`  (
  `menu_id` varchar(32) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_id` varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  `_read` tinyint(1) NOT NULL DEFAULT 0,
  `_create` tinyint(1) NOT NULL DEFAULT 0,
  `_update` tinyint(1) NOT NULL DEFAULT 0,
  `_delete` tinyint(1) NOT NULL DEFAULT 0,
  `_report` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'System',
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`menu_id`, `role_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('99.05', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('99.04', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('01', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('91.01', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('99', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('99.01', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('99.02', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `role_menu` VALUES ('99.03', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 1, 1, 1, 1, 1, '2020-09-27 19:28:30', 'Super Administrator', NULL, NULL, NULL, NULL, 0, 1);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `role_id` varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'System',
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 'Superadmin', 'Root access.', '2019-07-02 21:59:26', 'System', '2020-09-24 14:55:06', NULL, NULL, NULL, 0, 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` varchar(36) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `role_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hash` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `user_fullname` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `photo` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'no-photo.png',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `created_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'System',
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_by` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('45be257b-79e5-4927-abb8-6010593237fd', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 'superadmin@gmail.com', '$2y$12$wEo/ogiW/J64ln/skrzAJOCA2wqBBOOnangwxPjQuHd6y9o8rsXkW', 'Super Administrator', NULL, 'no-photo.png', NULL, 'System', '2020-09-24 15:11:32', NULL, NULL, NULL, 0, 1);
INSERT INTO `users` VALUES ('f21c065c-8d63-4501-8495-d14f72577c61', '5b0d053e-fe7e-48b2-bc82-dbc332c19e2a', 'user@gmail.com', '$2y$12$GB6xOthBZR410nyAR5.zq.H7YnYPY.Pxg.RTKfe/WUCA7ZMhX7kCm', 'User', '12345', 'b1423d7f026573754502669c56814584.jpg', '2020-09-24 15:24:09', 'Super Administrator', '2020-09-25 09:56:59', 'Super Administrator', NULL, NULL, 0, 1);

SET FOREIGN_KEY_CHECKS = 1;
