$(document).ready(function () {
  $('.modal-href').click(function (e) {
    e.preventDefault();
    var modal_title = $(this).attr("modal-title");
    var modal_size = $(this).attr("modal-size");
    var modal_custom_size = $(this).attr("modal-custom-size");
    var modal_header = $(this).attr("modal-header");
    var modal_content_top = $(this).attr("modal-content-top");

    $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

    $("#modal-title").html(modal_title);
    $("#modal-size").addClass('modal-' + modal_size);
    if (modal_custom_size) {
      $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
    }
    if (modal_content_top) {
      $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
    }
    if (modal_header == 'hidden') {
      $("#modal-header").addClass('d-none');
    } else {
      $("#modal-header").removeClass('d-none');
    }
    $("#myModal").modal('show');
    $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Memuat...</div>');
    $.post($(this).data('href'), function (data) {
      $("#modal-body").html(data.html);
    }, 'json');
  });
});

